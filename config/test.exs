use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :invoice_app, InvoiceAppWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn


# Databae configuration is in the test.secret.exs file
import_config "test.secret.exs"
