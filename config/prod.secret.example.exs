# Use this file as an example to generate prod.secret.exs file
# which will hold all secrets.

use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :invoice_app, InvoiceAppWeb.Endpoint,
  secret_key_base: "4P9shEeSAYnqyt9itfgCxXfdQU/9UO94rE9uWZnGMPivfVY6mfAaDf6HWK9fmHrF"

# Configure your database
config :invoice_app, InvoiceApp.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "invoice_app_prod",
  pool_size: 15
