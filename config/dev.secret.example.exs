# Use this file as an example to generate dev.secret.exs
use Mix.Config

# Configure your database
config :invoice_app, InvoiceApp.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "svetlozar",
  password: "",
  database: "invoice_app_test",
  hostname: "localhost",
  pool_size: 10
