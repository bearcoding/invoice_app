# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :invoice_app,
  ecto_repos: [InvoiceApp.Repo]

# Configures the endpoint
config :invoice_app, InvoiceAppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8jEtGyROqlULzznEBq0A4yIyZsZG9v5YSrW7pE1PGUqTTl8pjRwKjeLY+sHsLHO4",
  render_errors: [view: InvoiceAppWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: InvoiceApp.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
