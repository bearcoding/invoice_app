defmodule InvoiceApp.Repo.Migrations.CreateIssuerCompanies do
  use Ecto.Migration

  def change do
    create table(:issuer_companies) do
      add :name, :string, null: false
      add :address, :string, null: false
      add :uid, :string, null: false
      add :vat_uid, :string, null: false, default: ""
      add :accountable_person_name, :string, null: false

      timestamps()
    end

  end
end
