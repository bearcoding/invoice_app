defmodule InvoiceApp.Repo.Migrations.AddUniqueIndexToUidOfIssuerCompany do
  use Ecto.Migration

  def up do
    create unique_index(:issuer_companies, [:uid])
  end

  def down do
    drop unique_index(:issuer_companies, [:uid])
  end
end
