defmodule InvoiceApp.Repo.Migrations.AddFieldsToIssuerCompanies do
  use Ecto.Migration

  def change do
    alter table(:issuer_companies) do
      add :name_en, :string, null: false
      add :address_en, :string, null: false
      add :accountable_person_name_en, :string, null: false
    end
  end
end
