defmodule InvoiceApp.Repo.Migrations.AddRelationBetweenDeliveryNoteAndIssuerCompany do
  use Ecto.Migration

  def change do
    alter table(:delivery_notes) do
      add :issuer_company_id, references(:issuer_companies, on_delete: :restrict), null: false
    end

    create index(:delivery_notes, [:issuer_company_id])
  end
end
