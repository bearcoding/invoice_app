defmodule InvoiceApp.Repo.Migrations.CreateReceiverDetails do
  use Ecto.Migration

  def change do
    create table(:receiver_details) do
      add :accountable_person_name, :string, null: false, default: ""
      add :address, :string, null: false, default: ""
      add :name, :string, null: false, default: ""
      add :uid, :string, null: false, default: ""
      add :vat_uid, :string, null: false, default: "" 
      add :other_information, :string, null: false, default: ""
      add :legal_entity, :boolean, null: false

      timestamps()
    end

  end
end
