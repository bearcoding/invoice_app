defmodule InvoiceApp.Repo.Migrations.CreateDeliveryNotes do
  use Ecto.Migration

  def change do
    create table(:delivery_notes) do
      add :document_number, :integer, null: false
      add :issuing_date, :date, null: false
      add :tax_event_date, :date, null: false
      add :vat_percentage, :decimal, null: false
      add :issuer_name, :string, null: false, default: ""
      add :issuer_name_en, :string, null: false, default: ""
      add :receiver_name, :string, null: false, default: ""
      add :notes, :string, null: false, default: ""
      add :delivery_note_id, :integer
      add :discount_percentage, :decimal, null: false, default: 0

      timestamps()
    end

    create unique_index(:delivery_notes, :document_number)
  end
end
