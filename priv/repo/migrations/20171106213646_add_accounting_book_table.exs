defmodule InvoiceApp.Repo.Migrations.AddAccountingBookTable do
  use Ecto.Migration

  def change do
    create table(:accounting_books) do
      add :number, :integer, null: false
      add :description, :string, null: false

      timestamps()
    end

  end
end
