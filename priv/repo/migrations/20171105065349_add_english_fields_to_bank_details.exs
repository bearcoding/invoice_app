defmodule InvoiceApp.Repo.Migrations.AddEnglishFieldsToBankDetails do
  use Ecto.Migration

  def change do
    alter table(:bank_details) do
      add :bank_name_en, :string, null: false
    end
  end
end
