defmodule InvoiceApp.Repo.Migrations.CreateBankDetails do
  use Ecto.Migration

  def change do
    create table(:bank_details) do
      add :bank_name, :string, null: false
      add :iban, :string, null: false
      add :bic, :string, null: false
      add :issuer_company_id, references(:issuer_companies, on_delete: :restrict), null: false

      timestamps()
    end

    create unique_index(:bank_details, [:iban])
    create index(:bank_details, [:issuer_company_id])
  end
end
