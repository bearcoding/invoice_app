defmodule InvoiceApp.Repo.Migrations.Item do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :description, :string, null: false, default: ""
      add :description_en, :string, null: false, default: ""
      add :price, :decimal, null: false, default: 0
      add :quantity, :integer, null: false, default: 1
      add :delivery_note_id, references(:delivery_notes, on_delete: :delete_all)


      timestamps()
    end
    
    create index(:items, [:delivery_note_id])

  end
end
