defmodule InvoiceApp.Repo.Migrations.AddRelationBetweenDeliveryNoteAndAccountingBook do
  use Ecto.Migration

  def change do
    alter table(:delivery_notes) do
      add :accounting_book_id, references(:accounting_books, on_delete: :restrict), null: false 
    end

    unique_index(:delivery_notes, :accounting_book_id)

  end
end
