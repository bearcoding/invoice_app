defmodule InvoiceApp.Repo.Migrations.AddRelationBetweenDeliveryNoteAndReceiverDetails do
  use Ecto.Migration

  def change do
    alter table(:delivery_notes) do
       add :receiver_details_id, references(:receiver_details, on_delete: :restrict), null: false
    end

    create index(:delivery_notes, [:receiver_details_id])
  end

end
