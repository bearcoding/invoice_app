alias InvoiceApp.Repo
alias InvoiceApp.Accounting.IssuerCompany
alias InvoiceApp.Accounting.BankDetails

company = Repo.insert!(%IssuerCompany{
  accountable_person_name: "Светлозар Тодоров",
  accountable_person_name_en: "Svetlozar Todorov",
  address: "ул. Банат 3, ет. 1, ап. 3, София 1407, България",
  address_en: "Banat street 3, floor 1, app. 3, Sofia 1407, Bulgaria",
  name_en: "Neuvents LTD",
  name: "Нойвентс ООД",
  uid: "203844519",
  vat_uid: "BG203844519"
})

Repo.insert!(%BankDetails{
  bank_name: "Райфайзен Банк",
  bank_name_en: "Raiffaisen Bank",
  bic: "RZBBBGSF",
  iban: "BG79RZBB91551006674835",
  issuer_company_id: company.id
})
