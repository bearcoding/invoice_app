alias InvoiceApp.Repo
alias InvoiceApp.Accounting.DeliveryNote
alias InvoiceApp.Accounting.Item 

[delivery_note] = Repo.all(DeliveryNote)

Repo.insert!(%Item{delivery_note_id: delivery_note.id, 
               description: "Функционална Конференция", 
               description_en: "Func Conf", 
               price: Decimal.new(100), quantity: 5})

Repo.insert!(%Item{delivery_note_id: delivery_note.id,
               description: "Конференция хакер", 
               description_en: "hacker conf", 
               price: Decimal.new(60), quantity: 10})

