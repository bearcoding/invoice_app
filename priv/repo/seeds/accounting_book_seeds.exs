alias InvoiceApp.Repo
alias InvoiceApp.Accounting.AccountingBook
alias InvoiceApp.Accounting.DeliveryNote
alias InvoiceApp.Accounting.IssuerCompany
alias InvoiceApp.Accounting.ReceiverDetails


accounting_book = Repo.insert!(%AccountingBook{
  number: 1000010000, description: "OpenConf - Tickets"})

[company] = Repo.all(IssuerCompany)
[receiver_details] = Repo.all(ReceiverDetails)

Repo.insert!(%DeliveryNote{accounting_book_id: accounting_book.id,
  receiver_details_id: receiver_details.id, 
  tax_event_date: ~D[2017-11-12], issuer_name: "Александър Тенев", 
  issuer_name_en: "Aleksandar Tenev", issuing_date: ~D[2017-11-12], 
  document_number: accounting_book.number + 1, 
  receiver_name: "Svetlozar", notes: "Just a single ticket", 
  vat_percentage: Decimal.new(20), issuer_company_id: company.id})

  
