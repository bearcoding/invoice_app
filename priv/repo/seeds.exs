# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     InvoiceApp.Repo.insert!(%InvoiceApp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias InvoiceApp.Repo

reset_tables =  [
                  InvoiceApp.Accounting.DeliveryNote, 
                  InvoiceApp.Accounting.AccountingBook, 
                  InvoiceApp.Accounting.BankDetails, 
                  InvoiceApp.Accounting.IssuerCompany,
                  InvoiceApp.Accounting.ReceiverDetails,
                  InvoiceApp.Accounting.Item
                ]

load_tables = [
                "issuer_company_seeds.exs",
                "receiver_details_seeds.exs",
                "accounting_book_seeds.exs",
                "items_seeds.exs"
              ]

seeds_directory = __DIR__ <> "/seeds/"

reset_tables |> Enum.map(&Repo.delete_all(&1))
load_tables |> Enum.map(&Code.require_file(&1, seeds_directory))


