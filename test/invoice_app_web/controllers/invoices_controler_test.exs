defmodule InvoiceAppWeb.InvoicesControllerTest do
  use InvoiceAppWeb.ConnCase
  import Phoenix.View

  alias InvoiceApp.Repo
  alias InvoiceApp.Accounting
  alias InvoiceApp.Accounting.DeliveryNote
  alias InvoiceApp.Accounting.AccountingBook
  alias InvoiceApp.Accounting.ReceiverDetails
  alias InvoiceApp.Accounting.IssuerCompany
  alias InvoiceApp.Accounting.BankDetails

  setup [:create_invoice]
  test "invoices index responds with all Invoices", %{conn: conn} do


    invoices =  
      Accounting.delivery_notes |> 
      Accounting.load_receiver_details

    expected =
      render_to_string InvoiceAppWeb.InvoiceView,
      "index.html", invoices: invoices, conn: conn 

    response = conn
      |> get(invoice_path(conn, :index))
      |> html_response(200)

    assert response =~ expected
  end

  defp create_invoice(_) do
    
    accounting_book = Repo.insert!(%AccountingBook{
               number: 1000010009, description: "OpenConf - Tickets"})
    
    receiver_details =  Repo.insert!(%ReceiverDetails{accountable_person_name: "Tenev",
               address: "Vratsa, Seniche, block 69, entr. 1, apt.25",
               name: "Aleksandar Tenev - TEST ENV", uid: "1232132142",
               vat_uid: "12321321", legal_entity: true})

    company = Repo.insert!(%IssuerCompany{
              accountable_person_name: "Светлозар Тодоров",
              accountable_person_name_en: "Svetlozar Todorov",
              address: "ул. Банат 3, ет. 1, ап. 3, София 1407, България",
              address_en: "Banat street 3, floor 1, app. 3, Sofia 1407, Bulgaria",
              name_en: "Neuvents LTD",
              name: "Нойвентс ООД",
              uid: "203844519",
              vat_uid: "BG203844519"})

    Repo.insert!(%BankDetails{
              bank_name: "Райфайзен Банк",
              bank_name_en: "Raiffaisen Bank",
              bic: "RZBBBGSF",
              iban: "BG79RZBB91551006674835",
              issuer_company_id: company.id})

    invoice = Repo.insert!(%DeliveryNote{accounting_book_id: accounting_book.id,
        receiver_details_id: receiver_details.id, 
        tax_event_date: ~D[2017-11-12], issuer_name: "Александър Тенев", 
        issuer_name_en: "Aleksandar Tenev", issuing_date: ~D[2017-11-12], 
        document_number: accounting_book.number + 1, 
        receiver_name: "Svetlozar", notes: "Just a single ticket", 
        vat_percentage: Decimal.new(20), issuer_company_id: company.id})

    {:ok, invoice: invoice}
  end

end
