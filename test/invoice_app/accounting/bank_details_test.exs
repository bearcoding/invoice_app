defmodule InvoiceApp.Accounting.BankDetailsTest do
  use InvoiceApp.DataCase

  alias InvoiceApp.Accounting.BankDetails

  @invalid_attrs %{bank_name: nil, bic: nil, iban: "", issuer_company_id: nil}
  @valid_attrs %{
    bank_name: "some bank_name",
    bank_name_en: "some bank_name in english",
    bic: "some bic",
    iban: "some iban",
    issuer_company_id: 2
  }

  describe "changeset/2" do
    test "changeset/2 with valid parameters" do
      changeset = BankDetails.changeset(%BankDetails{}, @valid_attrs)
      # TODO: This does not really test if a record can be created.
      # issuer_company_id could be an id for a non existent record and it will
      # still work. Basically the changeset validates only presence.
      assert changeset.valid?
    end

    test "changeset/2 with invalid parameters" do
      changeset = BankDetails.changeset(%BankDetails{}, @invalid_attrs)
      refute changeset.valid?
    end
  end
end
