defmodule InvoiceApp.Accounting.IssuerCompanyTest do
  use InvoiceApp.DataCase

  alias InvoiceApp.Accounting.IssuerCompany

  @valid_attrs %{
    accountable_person_name: "some accountable_person_name",
    accountable_person_name_en: "some accountable_person_name in english",
    address: "some address",
    address_en: "some address in english",
    name: "some name",
    name_en: "some name in english",
    uid: "some uid",
    vat_uid: "some vat_uid"
  }

  @invalid_attrs %{accountable_person_name: nil, address: nil, name: nil, uid: nil, vat_uid: nil}

  describe "changeset/2" do
    test "changeset/2 with valid parameters" do
      changeset = IssuerCompany.changeset(%IssuerCompany{}, @valid_attrs)
      assert changeset.valid?
    end

    test "changeset/2 with invalid parameters" do
      changeset = IssuerCompany.changeset(%IssuerCompany{}, @invalid_attrs)
      refute changeset.valid?
    end

    test "changeset/2 with empty vat_uid" do
      changeset = IssuerCompany.changeset(%IssuerCompany{}, Map.put(@valid_attrs, :vat_uid, ""))
      assert changeset.valid?
    end
  end
end
