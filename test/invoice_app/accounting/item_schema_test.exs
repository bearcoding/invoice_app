defmodule InvoiceApp.Accounting.ItemTest do
  use InvoiceApp.DataCase
  alias InvoiceApp.Accounting.DeliveryNote
  alias InvoiceApp.Accounting.Item

  @valid_attributes %{description: "Конференция - Паршъл",
                      description_en: "Partial::conf"} 

  @invalid_attributes %{description: "Конференция",
                        description_en: "Partial::conf",
                        price: -1, quantity: -1}

  describe "changeset/2" do
   test "changeset/2 with correct input information" do
     changeset = %Item{}
                 |> Item.changeset(@valid_attributes)
     assert changeset.valid?
   end
  
   test "changeset/2 - missing required field" do
     changeset = %Item{}
                 |> Item.changeset(%{description_en: "Partial::conf"})
     refute changeset.valid?
   end

   test "changeset/2 when not all required inputs are provided" do
      changeset = %Item{}
                 |> Item.changeset(%{description: "Конференция",
                                    price: 10, quantity: 5})
     refute changeset.valid?
   end

   test "changeset/2 negative values" do
     changeset = %Item{}
                  |> Item.changeset(@invalid_attributes)
                  |> IO.inspect
     refute changeset.valid? 
   end
  end
  
  test "Item schema field default values" do
      assert %Item{description: "", description_en: "",
                  price: 0, quantity: 1} == %Item{}
  end


end
