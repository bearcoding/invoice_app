defmodule InvoiceApp.Accounting.AccountingBookTest do
  use InvoiceApp.DataCase
  alias InvoiceApp.Accounting.DeliveryNote
  alias InvoiceApp.Accounting.AccountingBook
  alias InvoiceApp.Repo


  @valid_attributes %{number: 1000,
                      description: "OpenFest 2017"}

  @invalid_attributes %{number: "", 
                        description: "OpenFest 2017"}

 
  describe "changeset/2" do
    test "changeset/2 with correct input information" do
      changeset = %AccountingBook{} |> AccountingBook.changeset(@valid_attributes)
      assert changeset.valid?
    end

    test "changeset/2 with invalid input information" do
      changeset = %AccountingBook{} |> AccountingBook.changeset(@invalid_attributes)
      refute changeset.valid?
    end
   
    test "changeset/2 with 10 digit integer" do
      changeset = %AccountingBook{} 
      |> AccountingBook.changeset(%{number: 1000010000, description: "conference"})
      |> IO.inspect
      assert changeset.valid?
    end
  end

end

