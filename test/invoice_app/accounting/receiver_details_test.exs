defmodule InvoiceApp.Accounting.ReceiverDetailsTest do
  use InvoiceApp.DataCase
  alias InvoiceApp.Accounting.ReceiverDetails

  
  
  @valid_attributes_company %{accountable_person_name: "тенев",
                              address: "София", 
                              name: "Тенев ООД",
                              uid: "123121231", vat_uid: "21312321",
                              legal_entity: true}

  @valid_attributes_individual %{address: "София", 
                              name: "Тенев",
                              legal_entity: false}

  @invalid_attributes %{legal_entity: :tenev}

 
  
  describe "changeset/2" do
   test "changeset/2 with correct company input information" do
     changeset = %ReceiverDetails{}
                 |> ReceiverDetails.changeset(@valid_attributes_company)
                 |> IO.inspect
     assert changeset.valid?
   end
   
   test "changeset/2 with correct individual input information" do
     changeset = %ReceiverDetails{}
                 |> ReceiverDetails.changeset(@valid_attributes_individual)
                 |> IO.inspect
     assert changeset.valid?
   end

   test "changeset/2 - incorrect input information" do
     changeset = %ReceiverDetails{}
                 |> ReceiverDetails.changeset(@invalid_attributes)
     refute changeset.valid?
   end
  end
  
end
