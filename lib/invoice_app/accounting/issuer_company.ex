defmodule InvoiceApp.Accounting.IssuerCompany do
  use Ecto.Schema
  import Ecto.Changeset
  alias InvoiceApp.Accounting.IssuerCompany

  @all_attributes [
    :name,
    :name_en,
    :address_en,
    :accountable_person_name_en,
    :address,
    :uid,
    :vat_uid,
    :accountable_person_name
  ]
  @required_attributes [
    :name,
    :name_en,
    :address_en,
    :accountable_person_name_en,
    :address,
    :uid,
    :accountable_person_name
  ]

  schema "issuer_companies" do
    field :accountable_person_name, :string
    field :accountable_person_name_en, :string
    field :address, :string
    field :address_en, :string
    field :name, :string
    field :name_en, :string
    field :uid, :string
    field :vat_uid, :string, default: ""

    timestamps()

    has_many :bank_details, InvoiceApp.Accounting.BankDetails
    has_many :delivery_notes, InvoiceApp.Accounting.DeliveryNote
  end

  @doc false
  def changeset(%IssuerCompany{} = issuer_company, attrs) do
    issuer_company
    |> cast(attrs, @all_attributes)
    |> validate_required(@required_attributes)
    |> unique_constraint(:uid)
  end
end
