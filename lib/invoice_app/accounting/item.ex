defmodule InvoiceApp.Accounting.Item do
  use Ecto.Schema
  import Ecto.Changeset
  alias InvoiceApp.Accounting.Item
  alias InvoiceApp.Accounting.DeliveryNote
  
  @moduledoc""" 
  Module containing schema for invoice items and related changeset
  """ 

  schema "items" do
    field :description, :string, default: ""
    field :description_en, :string, default: ""
    field :price, :decimal, default: 0
    field :quantity, :integer, default: 1

    # Associations

    belongs_to :delivery_note, DeliveryNote

    timestamps()
  end

  @doc false
  def changeset(%Item{} = item, attrs) do
    item
    |> cast(attrs, [:description, :description_en, :price, :quantity])
    |> validate_required([:description, :description_en, :price, :quantity])
    |> validate_number(:quantity, greater_than_or_equal_to: 1)
    |> validate_number(:price, greater_than_or_equal_to: 0)
  end
end
