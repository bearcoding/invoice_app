defmodule InvoiceApp.Accounting.DeliveryNote do
  use Ecto.Schema
  import Ecto.Changeset
  alias InvoiceApp.Accounting.DeliveryNote
  alias InvoiceApp.Accounting.Item

  schema "delivery_notes" do
    field :tax_event_date, :date
    field :total, :decimal, virtual: true
    field :issuer_name, :string, default: ""
    field :issuer_name_en, :string, default: ""
    field :issuing_date, :date
    field :document_number, :integer, default: 0
    field :receiver_name, :string, default: ""
    field :notes, :string, default: ""
    field :tax_base, :decimal, virtual: true
    field :invoice_amount, :decimal, virtual: true
    field :vat_amount, :decimal, virtual: true
    field :vat_percentage, :decimal
    field :delivery_note_id, :integer
    field :discount_percentage, :decimal, default: 0
    field :discount_amount, :decimal, default: 0, virtual: true

    # Associations
    
    has_many :items, Item                                   

    belongs_to :issuer_company, InvoiceApp.Accounting.IssuerCompany
    belongs_to :accounting_book, InvoiceApp.Accounting.AccountingBook
    belongs_to :receiver_details, InvoiceApp.Accounting.ReceiverDetails
    
    timestamps()
  end

  @doc false
  def changeset(%DeliveryNote{} = delivery_note, attrs) do
    delivery_note
    |> cast(attrs, [:tax_event_date, :total, :issuer_name, :issuer_name_en, :issuing_date,
                    :document_number, :receiver_name, :notes, :tax_base, :invoice_amount,
                    :vat_amount, :vat_percentage, :delivery_note_id, :discount_percentage,
                    :discount_amount])
    |> validate_required([:tax_event_date, :total, :issuer_name, :issuer_name_en, :issuing_date,
                    :document_number, :receiver_name, :notes, :tax_base, :invoice_amount,
                    :vat_amount, :vat_percentage, :delivery_note_id, :discount_percentage,
                    :discount_amount])
    |> foreign_key_constraint(:issuer_company_id)
  end
end
