defmodule InvoiceApp.Accounting.BankDetails do
  use Ecto.Schema
  import Ecto.Changeset
  alias InvoiceApp.Accounting.BankDetails

  @required_attributes [:bank_name, :bank_name_en, :bic, :iban, :issuer_company_id]

  schema "bank_details" do
    field :bank_name, :string
    field :bank_name_en, :string
    field :bic, :string
    field :iban, :string

    timestamps()

    belongs_to :issuer_company, InvoiceApp.Accounting.IssuerCompany
  end

  @doc false
  def changeset(%BankDetails{} = bank_details, attrs) do
    bank_details
    |> cast(attrs, @required_attributes)
    |> validate_required(@required_attributes)
    |> foreign_key_constraint(:issuer_company_id)
    |> unique_constraint(:iban)
  end
end
