defmodule InvoiceApp.Accounting.ReceiverDetails do
  use Ecto.Schema
  import Ecto.Changeset
  alias InvoiceApp.Accounting.ReceiverDetails
  alias InvoiceApp.Accounting.DeliveryNote


  schema "receiver_details" do
    field :accountable_person_name, :string
    field :address, :string                         # Used for both company and individual address
    field :name, :string                            # Used for both company and individual address
    field :other_information, :string
    field :uid, :string
    field :vat_uid, :string
    field :legal_entity, :boolean

    # Associations

    has_many :delivery_notes, DeliveryNote

    timestamps()
  end

  @doc false
  def changeset(%ReceiverDetails{} = receiver_details, %{legal_entity: false} = attrs) do
    receiver_details
    |> cast(attrs, [:address, :name,
                    :other_information,
                    :legal_entity])
    |> validate_required([:address, :name,
                          :legal_entity])
  end
  
  def changeset(%ReceiverDetails{} = receiver_details, %{legal_entity: true} = attrs) do
    receiver_details
    |> cast(attrs, [:accountable_person_name, 
                    :address, :name, :uid, :vat_uid,
                    :legal_entity])
    |> validate_required([:accountable_person_name,
                          :address, :name, :uid, :vat_uid,
                          :legal_entity])
  end

  def changeset(%ReceiverDetails{} = receiver_details, attrs) do
    receiver_details
    |> cast(attrs, [:legal_entity])
    |> validate_required([:legal_entity])
  end

end
