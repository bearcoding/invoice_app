defmodule InvoiceApp.Accounting do
  alias InvoiceApp.Repo
  alias InvoiceApp.Accounting.DeliveryNote
  alias InvoiceApp.Accounting.Item
  alias InvoiceApp.Accounting.IssuerCompany
  alias InvoiceApp.Accounting.ReceiverDetails
  alias InvoiceApp.Accounting.AccountingBook

  # Queries

  def delivery_notes do
    Repo.all(DeliveryNote)
  end

  def get_delivery_note(id) do
    Repo.get_by(DeliveryNote, id: id) 
  end

  # Load associations 
  
  def load_receiver_details (delivery_note) do
    Repo.preload(delivery_note, :receiver_details)
  end

  def load_accounting_book (delivery_note) do
    Repo.preload(delivery_note, :accounting_book) 
  end

  def load_issuer_company (delivery_note) do
    Repo.preload(delivery_note, :issuer_company) 
  end

  def load_bank_details (delivery_note) do
    Repo.preload(delivery_note.issuer_company, :bank_details)
  end

  def load_items (delivery_note) do
    Repo.preload(delivery_note, :items)
  end

  def load_receiver_details (delivery_note) do
    Repo.preload(delivery_note, :receiver_details) 
  end
  
  # Helper functions
  
 end
