defmodule InvoiceApp.Accounting.AccountingBook do
  use Ecto.Schema
  import Ecto.Changeset
  alias InvoiceApp.Accounting.AccountingBook
  alias InvoiceApp.Accounting.DeliveryNote

  @moduledoc """

  Module containing Acccount Book details. 
  Up to 5 Account Books could be created. 

  accountbook_number is the starting number for a given Account Book and 
  its related Invoices and Credit Notes

  """


  schema "accounting_books" do
    field :number, :integer
    field :description, :string

    # Associations

    has_many :delivery_notes, DeliveryNote

    timestamps()
  end

  @doc false
  def changeset(%AccountingBook{} = accounting_book, attrs) do
    accounting_book
    |> cast(attrs, [:description, :number])
    |> validate_required([:number, :description])
  end
end
