defmodule InvoiceAppWeb.InvoiceView do
  use InvoiceAppWeb, :view
  
    
  def calculate_invoice_amount (invoice_items) do
    Enum.reduce(invoice_items, Decimal.new(0), fn(item, acc) ->
      sum = multiply(item.price, item.quantity)
      Decimal.add(Decimal.new(acc), Decimal.new(sum)) end)
  end

  def calculate_vat(invoice_items, vat) do
    invoice_items
    |> calculate_invoice_amount
    |> Decimal.mult(Decimal.mult(Decimal.new(0.01), vat))
  end

  def multiply(price, quantity) do
    Decimal.mult(price, Decimal.new(quantity))
  end
 
end
