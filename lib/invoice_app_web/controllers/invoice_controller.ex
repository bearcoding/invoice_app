defmodule InvoiceAppWeb.InvoiceController do
  use InvoiceAppWeb, :controller
  alias InvoiceApp.Accounting

  def index(conn, _params) do
    invoices = 
      Accounting.delivery_notes |>
      Accounting.load_receiver_details 
      
    render conn, "index.html", invoices: invoices
  end

  def show(conn, %{"id" => id}) do
    
    invoice = id
    |> Accounting.get_delivery_note
    |> Accounting.load_accounting_book
    |> Accounting.load_issuer_company
    |> Accounting.load_items
    |> Accounting.load_receiver_details

    company_details = 
    invoice
    |> Accounting.load_bank_details

    [ bank_details | _ ] = company_details.bank_details

    render conn, "show.html", invoice: invoice, 
                  bank_details: bank_details 
  end

end
